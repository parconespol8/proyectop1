
package CompraVentaAuto.Interfaz;


public class OpcionMenu {
    
    public static final int MENU_PRINCIPAL = 1;
    public static final int MENU_VENDEDOR = 2;
    public static final int MENU_COMPRADOR = 3;
    
    public static final int OP_PRINC_VENDEDOR = 1;
    public static final int OP_PRINC_COMPRADOR = 2;
    public static final int OP_PRINC_SALIR = 3;
    
    public static final int OP_VEND_REGVEND = 1;
    public static final int OP_VEND_REGVEH = 2;
    public static final int OP_VEND_ACEPTAOFER = 3;
    public static final int OP_VEND_REGRESAR = 4;
    
    public static final int OP_CMP_REGCMP = 1;
    public static final int OP_CMP_REGOFERTA = 2;
    public static final int OP_CMP_REGRESAR = 3;
    
    private int _menu;
    private int _opcionMenu;
    private Object _datosDevuelto;
    
    
    public OpcionMenu()
    {
        
    }
    
    public int getMenu() {
        return _menu;
    }

    public void setMenu(int _menu) {
        this._menu = _menu;
    }

    public int getOpcionMenu() {
        return _opcionMenu;
    }

    public void setOpcionMenu(int _opcionMenu) {
        this._opcionMenu = _opcionMenu;
    }

    public Object getDatosDevuelto() {
        return _datosDevuelto;
    }

    public void setDatosDevuelto(Object _datosDevuelto) {
        this._datosDevuelto = _datosDevuelto;
    }
}
