
package CompraVentaAuto.Interfaz;

import CompraVentaAuto.Clases.Comprador;
import CompraVentaAuto.Clases.OfertaCompra;
import CompraVentaAuto.Clases.PersonaVtaCmp;
import CompraVentaAuto.Clases.Vehiculo;
import CompraVentaAuto.Clases.Vendedor;
import CompraVentaAuto.Herramienta.EnvioEmail;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;



public class PantallaConsola 
{
    private final String TABDOBLE = "\t\t";
    private Scanner consola;
    private String emailEmisor;
    private String emailEmisorClave;
    ArrayList<Comprador> lstCompradores;
    ArrayList<Vendedor> lstVendedores;
    ArrayList<Vehiculo> lstVehiculos;
    ArrayList<OfertaCompra> lstOferta;
    Vendedor _vendedorSesion;
    Comprador _compradorSesion;
   
    
    public PantallaConsola(String ctaMailEmisor, String claveMailEmisor,
                    ArrayList<Comprador> compradores, ArrayList<Vendedor> vendedores,
                    ArrayList<Vehiculo> vehiculos, ArrayList<OfertaCompra> ofertas)
    {
        this.emailEmisor = ctaMailEmisor;
        this.emailEmisorClave = claveMailEmisor;
        this.lstCompradores = compradores;
        this.lstVehiculos = vehiculos;
        this.lstVendedores = vendedores;
        this.lstOferta = ofertas;
        consola = new Scanner(System.in);
    }
    
    public void imprimirMensaje(String mensaje)
    {
        System.out.println(mensaje);
    }
    public void imprimirMensajeSinSalto(String mensaje)
    {
        System.out.print(mensaje);
    }
    
    public OpcionMenu EjecutarOpcion(int menu,int opcion)
    {
        OpcionMenu opcionElejida = new OpcionMenu();
        int opcionMenu = 0;
        if(menu==OpcionMenu.MENU_PRINCIPAL)
        {
            switch(opcion)
            {
                case OpcionMenu.OP_PRINC_VENDEDOR:
                    opcionMenu = OpcionMenuVendedor();
                    opcionElejida.setMenu(OpcionMenu.MENU_VENDEDOR);
                    opcionElejida.setOpcionMenu(opcionMenu);
                    break;
                case OpcionMenu.OP_PRINC_COMPRADOR:
                    opcionMenu = OpcionMenuComprador();
                    opcionElejida.setMenu(OpcionMenu.MENU_COMPRADOR);
                    opcionElejida.setOpcionMenu(opcionMenu);
                    break;
                case OpcionMenu.OP_PRINC_SALIR:
                    opcionElejida = null;
                    break;
                default:
                    opcionMenu = OpcionMenuPrincipal();
                    opcionElejida.setMenu(OpcionMenu.MENU_PRINCIPAL);
                    opcionElejida.setOpcionMenu(opcionMenu);
            }
        }
        if(menu==OpcionMenu.MENU_VENDEDOR)
        {
            ArrayList<OfertaCompra> ofertasAceptadas = null;
            switch(opcion)
            {
                case OpcionMenu.OP_VEND_REGVEND:
                    ArrayList<Vendedor> nuevosVendedores = OpcionRegistroVendedor(this.lstVendedores);
                    opcionElejida.setMenu(OpcionMenu.MENU_VENDEDOR);
                    opcionElejida.setOpcionMenu(0);
                    break;
                case OpcionMenu.OP_VEND_REGVEH:
                    ArrayList<Vehiculo> nuevosVehiculos = OpcionRegistroVehiculo(this.lstVehiculos);
                    opcionElejida.setMenu(OpcionMenu.MENU_VENDEDOR);
                    opcionElejida.setOpcionMenu(0);
                    break;
                case OpcionMenu.OP_VEND_ACEPTAOFER:
                    _vendedorSesion = (Vendedor)getLogin(lstVendedores,PersonaVtaCmp.TIPO_VENDEDOR);
                    if(_vendedorSesion!=null)
                        ofertasAceptadas = OpcionAceptarOferta(this.lstOferta);
                    opcionElejida.setMenu(OpcionMenu.MENU_VENDEDOR);
                    opcionElejida.setOpcionMenu(0);
                    break;
                case OpcionMenu.OP_VEND_REGRESAR:
                    opcionElejida.setMenu(OpcionMenu.MENU_PRINCIPAL);
                    opcionElejida.setOpcionMenu(0);
                    break;
                default:
                    opcionMenu = OpcionMenuVendedor();
                    opcionElejida.setMenu(OpcionMenu.MENU_VENDEDOR);
                    opcionElejida.setOpcionMenu(opcionMenu);
            }
        }
        if(menu==OpcionMenu.MENU_COMPRADOR)
        {
            ArrayList<OfertaCompra> ofertasNuevas = null;
            switch(opcion)
            {
                case OpcionMenu.OP_CMP_REGCMP:
                    ArrayList<Comprador> nuevosCompradores = OpcionRegistroComprador(this.lstCompradores);
                    opcionElejida.setMenu(OpcionMenu.MENU_COMPRADOR);
                    opcionElejida.setOpcionMenu(0);
                    break;
                case OpcionMenu.OP_CMP_REGOFERTA:
                    _compradorSesion = (Comprador)getLogin(lstCompradores,PersonaVtaCmp.TIPO_COMPRADOR);
                    if(_compradorSesion!=null)
                        ofertasNuevas = OpcionIngresarOferta(this.lstOferta,_compradorSesion,this.lstVehiculos);
                    opcionElejida.setMenu(OpcionMenu.MENU_COMPRADOR);
                    opcionElejida.setOpcionMenu(0);
                    break;
                case OpcionMenu.OP_CMP_REGRESAR:
                    opcionElejida.setMenu(OpcionMenu.MENU_PRINCIPAL);
                    opcionElejida.setOpcionMenu(0);
                    break;
                default:
                    opcionMenu = OpcionMenuComprador();
                    opcionElejida.setMenu(OpcionMenu.MENU_COMPRADOR);
                    opcionElejida.setOpcionMenu(opcionMenu);
            }
        }
        
        return opcionElejida;
    }
    public PersonaVtaCmp getLogin(ArrayList personas, int tipoPersona)
    {
        String usuario = null;
        String clave = null;
        PersonaVtaCmp p = null;
        String resp ="";
        boolean continuar;
        do
        {
            continuar =false;
            imprimirMensaje("**************** ACCESO DE USUARIO **************************");
            imprimirMensajeSinSalto("Usuario: ");
            usuario = consola.nextLine();
            imprimirMensajeSinSalto("Clave: ");
            clave = consola.nextLine();
            try
            {
                p =PersonaVtaCmp.EjecLogin(personas, usuario, clave, tipoPersona);
            }
            catch(Exception ex)
            {
                imprimirMensaje(ex.getMessage());
                continuar =true;
            }
            if(p==null && !continuar)
            {
                imprimirMensaje("Error: usuario o contraseña incorrecta");
                do
                {
                    imprimirMensajeSinSalto("Desea volver a intentar(S|N): ");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("S"))
                    continuar =true;
            }
        }while(continuar);
        
        return p;
    }
    
    public int OpcionMenuPrincipal()
    {
        int opcion =-1;
        LimpiarPantalla();
        imprimirMensaje("**************** BIENVENIDOS AL SISTEMA DE OFERTAS PARA VEHICULOS *******************************");
        imprimirMensaje("1. Vendedor");
        imprimirMensaje("2. Comprador");
        imprimirMensaje("3. Salir");
        
        do{
            imprimirMensajeSinSalto("Teclee una opción: ");
            String op = consola.nextLine();
            op = op.trim();
            if(Character.isDigit(op.charAt(0)))
               opcion = Integer.parseInt(op);
            else
                imprimirMensaje("Debe teclear 1 o 2 o 3: ");
            
            if(!(opcion>0 && opcion<4) && opcion!=-1)
                imprimirMensaje("Debe teclear 1 o 2 o 3: ");
            
        }while(opcion<=0);
        
        return opcion;
    }
    
    public int OpcionMenuVendedor()
    {
        int opcion =-1;
        LimpiarPantalla();
        imprimirMensaje("********************** MENU VENDEDOR *******************************");
        imprimirMensaje("1. Registrar nuevo vendedor");
        imprimirMensaje("2. Ingresar nuevo vehículo");
        imprimirMensaje("3. Aceptar Oferta");
        imprimirMensaje("4. Regresar");
        
        do{
            imprimirMensajeSinSalto("Teclee una opción: ");
            String op = consola.nextLine();
            op = op.trim();
            if(Character.isDigit(op.charAt(0)))
               opcion = Integer.parseInt(op);
            else
                imprimirMensaje("Debe teclear 1 o 2 o 3 o 4: ");
            
            if(!(opcion>0 && opcion<5) && opcion!=-1)
                imprimirMensaje("Debe teclear 1 o 2 o 3 o 4: ");
            
        }while(opcion<=0);
        
        return opcion;
    }
    public int OpcionMenuComprador()
    {
        int opcion =-1;
        LimpiarPantalla();
        imprimirMensaje("********************** MENU COMPRADOR *******************************");
        imprimirMensaje("1. Registrar nuevo comprador");
        imprimirMensaje("2. Ofertar nuevo vehículo");
        imprimirMensaje("3. Regresar");
        
        do{
            imprimirMensajeSinSalto("Teclee una opción: ");
            String op = consola.nextLine();
            op = op.trim();
            if(Character.isDigit(op.charAt(0)))
               opcion = Integer.parseInt(op);
            else
                imprimirMensaje("Debe teclear 1 o 2 o 3: ");
            
            if(!(opcion>0 && opcion<5) && opcion!=-1)
                imprimirMensaje("Debe teclear 1 o 2 o 3: ");
            
        }while(opcion<=0);
        
        return opcion;
    }
    
    public ArrayList<Vendedor> OpcionRegistroVendedor(ArrayList<Vendedor> vendedores)
    {
        ArrayList<Vendedor> nuevos = new ArrayList<>();
        Vendedor vend = null;
        boolean continuar =true;
        
        LimpiarPantalla();
        
        do{
            imprimirMensaje("********************** REGISTRAR NUEVO VENDEDOR *******************************");
            vend= new Vendedor();
            imprimirMensajeSinSalto("Nombres: ");
            vend.setNombres(consola.nextLine());
            imprimirMensajeSinSalto("Apellidos: ");
            vend.setApellidos(consola.nextLine());
            imprimirMensajeSinSalto("E-mail: ");
            vend.setEmail(consola.nextLine());
            imprimirMensajeSinSalto("Organización: ");
            vend.setOrganizacion(consola.nextLine());
            imprimirMensajeSinSalto("Usuario: ");
            vend.setUsuario(consola.nextLine());
            imprimirMensajeSinSalto("Clave: ");
            vend.setClave(consola.nextLine());
            
            String msjExiste = PersonaVtaCmp.ExistePersona(vendedores, vend);
            if(msjExiste!=null)
            {
                String resp = "";
                imprimirMensaje(msjExiste);
                do
                {
                    imprimirMensajeSinSalto("¿Desea volver a ingresar los datos?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
            else
            {
                String resp = "";
                String msgRegistro  = vend.Registrar();
                if(msgRegistro!=null)
                    imprimirMensajeSinSalto(msgRegistro);
                else
                {
                    vendedores.add(vend);
                    nuevos.add(vend);
                }
                
                do
                {       
                    imprimirMensajeSinSalto("¿Desea ingresar otro vendedor?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
        }while(continuar);
        
        return nuevos;
    }
    public ArrayList<Comprador> OpcionRegistroComprador(ArrayList<Comprador> compradores)
    {
        ArrayList<Comprador> nuevos = new ArrayList<>();
        Comprador cmp = null;
        boolean continuar =true;
        
        LimpiarPantalla();
        imprimirMensaje("********************** REGISTRAR NUEVO COMPRADOR *******************************");
        
        do{
            cmp= new Comprador();
            imprimirMensajeSinSalto("Nombres: ");
            cmp.setNombres(consola.nextLine());
            imprimirMensajeSinSalto("Apellidos: ");
            cmp.setApellidos(consola.nextLine());
            imprimirMensajeSinSalto("E-mail: ");
            cmp.setEmail(consola.nextLine());
            imprimirMensajeSinSalto("Organización: ");
            cmp.setOrganizacion(consola.nextLine());
            imprimirMensajeSinSalto("Usuario: ");
            cmp.setUsuario(consola.nextLine());
            imprimirMensajeSinSalto("Clave: ");
            cmp.setClave(consola.nextLine());
            
            String msjExiste = PersonaVtaCmp.ExistePersona(compradores, cmp);
            if(msjExiste!=null)
            {
                String resp = "";
                imprimirMensaje(msjExiste);
                do
                {
                    imprimirMensajeSinSalto("¿Desea volver a ingresar los datos?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
            else
            {
                String resp = "";
                String msgRegistro  = cmp.Registrar();
                if(msgRegistro!=null)
                    imprimirMensajeSinSalto(msgRegistro);
                else
                {
                    compradores.add(cmp);
                    nuevos.add(cmp);
                }
                do
                {
                    imprimirMensajeSinSalto("¿Desea ingresar otro comprador?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
        }while(continuar);
        
        return nuevos;
    }
    public ArrayList<Vehiculo> OpcionRegistroVehiculo(ArrayList<Vehiculo> vehiculos)
    {
        ArrayList<Vehiculo> nuevos = new ArrayList<>();
        Vehiculo veh = null;
        boolean continuar =true;
        
        LimpiarPantalla();
        imprimirMensaje("********************** REGISTRAR NUEVO VEHICULO *******************************");
        
        do{
            veh= new Vehiculo();
            imprimirMensajeSinSalto("Tipo Vehiculo(AUTO,CAMIONETA,CAMION,MOTO): ");
            veh.setTipo(consola.nextLine());
            imprimirMensajeSinSalto("Placa: ");
            veh.setPlaca(consola.nextLine());
            imprimirMensajeSinSalto("Marca: ");
            veh.setMarca(consola.nextLine());
            imprimirMensajeSinSalto("Modelo: ");
            veh.setModelo(consola.nextLine());
            imprimirMensajeSinSalto("Tipo motor: ");
            veh.setTipoMotor(consola.nextLine());
            
            do
            {
                try
                {
                    imprimirMensajeSinSalto("Año: ");
                    veh.setAnio(Integer.parseInt(consola.nextLine()));
                }
                catch(NumberFormatException ex)
                {
                    imprimirMensaje("El AÑO debe ser un numero, intente de nuevo");
                }
            }while(veh.getAnio()==0);
            do
            {
                try
                {
                    imprimirMensajeSinSalto("Recorrido: ");
                    veh.setRecorrido(Integer.parseInt(consola.nextLine()));
                }
                catch(NumberFormatException ex)
                {
                    imprimirMensaje("El RECORRIDO debe ser un numero, intente de nuevo");
                }
            }while(veh.getRecorrido()==0);
            
            
            imprimirMensajeSinSalto("Color: ");
            veh.setColor(consola.nextLine());
            
            imprimirMensajeSinSalto("Tipo combustible: ");
            veh.setTipoCombustible(consola.nextLine());
            
            imprimirMensajeSinSalto("Vidrios: ");
            veh.setVidrios(consola.nextLine());
            
            imprimirMensajeSinSalto("Transmisión: ");
            veh.setTransmision(consola.nextLine());
            
            do
            {
                try
                {
                    imprimirMensajeSinSalto("Precio: ");
                    veh.setPrecio(Double.parseDouble(consola.nextLine()));
                }
                catch(NumberFormatException ex)
                {
                    imprimirMensaje("El Precio debe ser un valor númerico, intente de nuevo");
                }
            }while(veh.getPrecio()<1);
            
            String msjExiste = Vehiculo.ExisteVehiculo(vehiculos, veh);
            if(msjExiste!=null)
            {
                String resp = "";
                imprimirMensaje(msjExiste);
                do
                {
                    imprimirMensajeSinSalto("¿Desea volver a ingresar los datos?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
            else
            {
                String resp = "";
                
                String msgRegistro  = veh.Registrar();
                if(msgRegistro!=null)
                    imprimirMensajeSinSalto(msgRegistro);
                else
                {
                    vehiculos.add(veh);
                    nuevos.add(veh);
                }
                do
                {
                    imprimirMensajeSinSalto("¿Desea ingresar otro vehículo?(S|N):");
                    resp = consola.nextLine();
                }while(!resp.equalsIgnoreCase("S") && !resp.equalsIgnoreCase("N"));
                if(resp.equalsIgnoreCase("N"))
                    continuar =false;
            }
        }while(continuar);
        
        return nuevos;
    }
    public ArrayList<OfertaCompra> OpcionAceptarOferta(ArrayList<OfertaCompra> ofertas)
    {
        boolean continuarDo = false;
        ArrayList<OfertaCompra> aceptadas = new ArrayList<>();
        ArrayList<OfertaCompra> ofertasPlaca = null;
        String placaBusq = null;
        LimpiarPantalla();
        imprimirMensaje("********************** Aceptar Ofertas *******************************");
        do
        {
            imprimirMensajeSinSalto("Digite la placa: ");
            placaBusq = consola.nextLine();
            ofertasPlaca = OfertaCompra.GetOfertasxPlaca(ofertas, placaBusq);
            if(ofertasPlaca==null)
            {
                String resp = "";
                imprimirMensaje("No existen ofertas sin aceptar para el vehículo " + placaBusq);
                imprimirMensajeSinSalto("¿Desea volver a buscar?(S|N):");
                resp = consola.nextLine();
                if(resp.equalsIgnoreCase("S"))
                    continuarDo=true;
            }
        }while(continuarDo);
        
        if(ofertasPlaca!=null && ofertasPlaca.size()>0)
        {
            int indiceSig = 0;
            imprimirMensaje(" "  + ofertasPlaca.get(0).getVehiculo().getMarca() + " " + 
                            ofertasPlaca.get(0).getVehiculo().getModelo() +
                            " Precio: $" + ofertasPlaca.get(0).getVehiculo().getPrecio());
            imprimirMensaje("Se han realizado " + ofertasPlaca.size() +  " ofertas");
            do
            {
                imprimirMensaje("");
                imprimirMensaje("******** OFERTA #" + (indiceSig + 1) + "*******************");
                imprimirMensaje("Correo: " + ofertasPlaca.get(indiceSig).getOfertante().getEmail());
                imprimirMensaje("Precio Ofertado: " + ofertasPlaca.get(indiceSig).getValor());
                imprimirMensaje("");
                
                int opcion = 0;
                boolean incluidoOp1 = false;
                boolean incluidoOp2 = false;
               
                    if((indiceSig+1)<ofertasPlaca.size())
                    {
                        imprimirMensaje("1. Siguiente");
                        incluidoOp1 = true;
                    }
                    
                    if(indiceSig>0)
                    {
                        incluidoOp2 = true;
                        imprimirMensaje("2. Anterior");
                    }

                    imprimirMensaje("3. Aceptar");
                    imprimirMensaje("4. Salir");
                do
                {
                    imprimirMensajeSinSalto("Escoja una opción: ");
                    String op = consola.nextLine();
                    if(Character.isDigit(op.charAt(0)))
                        opcion = Integer.parseInt(op);
                     else
                    {
                         imprimirMensaje("Debe teclear una opción válida ");
                         opcion=-1;
                    }
                    
                    if(!(opcion==3 || opcion==4 || (opcion==1 && incluidoOp1) || (opcion==2 && incluidoOp2)))
                    {
                        opcion=-1;
                        imprimirMensaje("Debe teclear una opción válida ");
                    }
                }while(opcion==-1);
                
                switch(opcion)
                {
                    case 1: 
                        indiceSig++;
                        break;
                    case 2:
                        indiceSig--;
                        break;
                    case 3:
                        ofertasPlaca.get(indiceSig).setAceptada(true);
                        aceptadas.add(ofertasPlaca.get(indiceSig));
                        EnvioEmail envio = new EnvioEmail(emailEmisor, emailEmisorClave);
                        envio.enviarByGMail(ofertasPlaca.get(indiceSig).getOfertante().getEmail(), 
                                ofertasPlaca.get(indiceSig).generarAsuntoAcepta(), 
                                ofertasPlaca.get(indiceSig).generarCuerpoMailAcepta());
                        ofertasPlaca.remove(indiceSig);
                        if(indiceSig>0)
                            indiceSig--;
                        break;
                    case 4:
                        indiceSig = ofertasPlaca.size()+1;
                }
            }while(indiceSig<ofertasPlaca.size());
            
            OfertaCompra.ActualizarBaseOfertas(ofertas);
        }
        
            
        
        return aceptadas;
    }
    public ArrayList<OfertaCompra> OpcionIngresarOferta(ArrayList<OfertaCompra> ofertas,Comprador personaCompra,ArrayList<Vehiculo> vehiculos)
    {
        
        ArrayList<OfertaCompra> nuevas = null;
        String[] criterios = null;
        int indiceTemp = 0;
        
        imprimirMensaje("*************** OFERTAS PARA VEHICULOS *******************");
        criterios = GetCriteriosOfertas();
        ArrayList<Vehiculo> vehFiltrado= Vehiculo.GetVehiculosxCriterios(vehiculos, criterios[0], Integer.parseInt(criterios[1]),
                Integer.parseInt(criterios[2]), Integer.parseInt(criterios[3]), Integer.parseInt(criterios[4]),
                Double.parseDouble(criterios[5]), Double.parseDouble(criterios[6]));
        
        if(vehFiltrado!=null && vehFiltrado.size()>0)
        {
            imprimirMensaje("Se encontraron " + vehFiltrado.size() + " vehículos\n");
            do
            {
                int opcion = GetOpcionOfertaVehiculo(vehFiltrado.get(indiceTemp), indiceTemp);
                switch(opcion)
                {
                    case 1:
                        if((indiceTemp+1)>=vehFiltrado.size())
                            imprimirMensaje("No se puede avanzar mas ya que no existen mas vehículos");
                        else
                            indiceTemp++;
                        break;
                    case 2:
                        if((indiceTemp-1)<=0)
                            imprimirMensaje("No se puede retrocer ya que se está presentando el primer vehiculo");
                        else
                            indiceTemp--;
                        break;
                    case 3:
                        OfertaCompra oferta = GetObtenerOferta(personaCompra,vehFiltrado.get(indiceTemp));
                        String msgResp = oferta.Registrar();
                        if(msgResp==null)
                        {
                            ofertas.add(oferta);
                        }
                        break;
                    case 4:
                        indiceTemp = vehFiltrado.size()+1;
                }
            }while(indiceTemp<vehFiltrado.size());
        }
        
        return nuevas;
    }
    
    private String[] GetCriteriosOfertas()
    {
        String strTemp = null;
        String[] criterios = new String[7];
        imprimirMensaje("Buscar por los siguientes criterios: ");
        imprimirMensajeSinSalto("Tipo de vehículo: ");
        criterios[0] = consola.nextLine();
        
        do
        {
            imprimirMensajeSinSalto("Recorrido desde:");
            strTemp = consola.nextLine();
        }while(!EsEntero(strTemp) && strTemp.length()>0);
        criterios[1] = (strTemp.trim().equals("")?"0":strTemp.trim());
        
        do
        {
            imprimirMensajeSinSalto("Recorrido hasta:");
            strTemp = consola.nextLine();
        }while(!EsEntero(strTemp) && strTemp.length()>0);
        criterios[2] = (strTemp.trim().equals("")?"0":strTemp.trim());
        
        do
        {
            imprimirMensajeSinSalto("Año desde:");
            strTemp = consola.nextLine();
        }while(!EsEntero(strTemp) && strTemp.length()>0);
        criterios[3] = (strTemp.trim().equals("")?"0":strTemp.trim());
        
        do
        {
            imprimirMensajeSinSalto("Año hasta:");
            strTemp = consola.nextLine();
        }while(!EsEntero(strTemp) && strTemp.length()>0);
        criterios[4] = (strTemp.trim().equals("")?"0":strTemp.trim());
        
        do
        {
            imprimirMensajeSinSalto("Precio desde:");
            strTemp = consola.nextLine();
        }while(!EsDecimal(strTemp) && strTemp.length()>0);
        criterios[5] = (strTemp.trim().equals("")?"0.00":strTemp.trim());
        
        do
        {
            imprimirMensajeSinSalto("Precio hasta:");
            strTemp = consola.nextLine();
        }while(!EsDecimal(strTemp) && strTemp.length()>0);
        criterios[6] = (strTemp.trim().equals("")?"0.00":strTemp.trim());
        
        return criterios;
    }
    
    private int GetOpcionOfertaVehiculo(Vehiculo vehCandidato, int indice)
    {
        int opcion = 0;
        LimpiarPantalla();
        imprimirMensaje("********************** Vehiculo  # "+ (indice+1) +"*******************************");
        imprimirMensajeSinSalto("Tipo Vehiculo: " + vehCandidato.getTipo());
        imprimirMensajeSinSalto(TABDOBLE);
        imprimirMensaje("Marca: " + vehCandidato.getMarca());
        
        imprimirMensajeSinSalto("Modelo: " + vehCandidato.getModelo());
        imprimirMensajeSinSalto(TABDOBLE);
        imprimirMensaje("Año: " + vehCandidato.getAnio());
        
        imprimirMensajeSinSalto("Recorrido: " + vehCandidato.getRecorrido());
        imprimirMensajeSinSalto(TABDOBLE);
        imprimirMensaje("Color: " + vehCandidato.getColor());
        
        imprimirMensajeSinSalto("Transmisión: " + vehCandidato.getTransmision());
        imprimirMensajeSinSalto(TABDOBLE);
        imprimirMensaje("Precio: $" + vehCandidato.getPrecio());
        
        imprimirMensaje("");
        imprimirMensaje("1.Siguiente    2.Anterior    3.Ofertar     4.Salir");
        do
        {
            imprimirMensaje("");
            imprimirMensajeSinSalto("Escoja una opción: ");
            String strOpcion = consola.nextLine();
            if(EsEntero(strOpcion))
                opcion = Integer.parseInt(strOpcion);
        }while(!(opcion>0 && opcion<4));
        
        return opcion;
    }
    private OfertaCompra GetObtenerOferta(Comprador personaCompra,Vehiculo veh)
    {
        OfertaCompra oferta = new OfertaCompra();
        do
        {
            imprimirMensaje("");
            imprimirMensajeSinSalto("Valor ofertado: ");
            String valor = consola.nextLine();
            if(EsDecimal(valor))
                oferta.setValor(Double.parseDouble(valor));
        }while(oferta.getValor()<=0);
        oferta.setAceptada(false);
        oferta.setOfertante(personaCompra);
        oferta.setVehiculo(veh);
        return oferta;
    }
    
    private void LimpiarPantalla()
    {
        try
        {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        }
        catch(Exception ex)
        {
            
        }
    }
    
    private boolean EsEntero(String valor)
    {
        boolean resp = true;
        try
        {
            Integer.parseInt(valor);
        }
        catch(Exception ex)
        {
            resp =false;
        }
        return resp;
    }
    private boolean EsDecimal(String valor)
    {
        boolean resp = true;
        try
        {
            Double.parseDouble(valor);
        }
        catch(Exception ex)
        {
            resp =false;
        }
        return resp;
    }
}
