
package CompraVentaAuto.Clases;

import CompraVentaAuto.Herramienta.CodificadorSHA;
import CompraVentaAuto.Herramienta.ProcesadorArchivo;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class Vendedor extends PersonaVtaCmp{

    private final String ARCHIVO = "VENDEDOR.CSV";
    private String _directorio = "";
    
    
    public Vendedor()
    {
        this._directorio = new File("").getAbsolutePath()+ "\\ARCHIVOS";
        this._tipoPersona = super.TIPO_VENDEDOR;
        this._procesadorFile = new ProcesadorArchivo(_directorio, ARCHIVO, this);
        
    }

    @Override
    public Object ConvertLineaToClase(String lineaArchivo) {
        Vendedor vend = null;
        
        if(lineaArchivo!=null && !lineaArchivo.trim().equals(""))
        {
            String[] datos = lineaArchivo.split(";");
            vend = new Vendedor();
            vend._nombres = datos[0];
            vend._apellidos = datos[1];
            vend._organizacion = datos[2];
            vend._email = datos[3];
            vend._usuario = datos[4];
            vend._clave = datos[5];
        }
        return vend;
    }

    @Override
    public ArrayList GetArrayListVacioInstancia() {
        return new ArrayList<Vendedor>();
    }

    @Override
    public ArrayList GetListArchivoDBClase() {
        ArrayList<Vendedor> lista = null;
        lista = _procesadorFile.GetListadoClases();
        if(lista==null)
            lista = new ArrayList<>();
        return lista;
    }
    
}
