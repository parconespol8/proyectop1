
package CompraVentaAuto.Herramienta;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;


public class ProcesadorArchivo 
{
    private String _urlArchivo;
    private boolean _abierto;
    private ConvertFileToClass _convertidor;
    
    public ProcesadorArchivo(String directorio, String nombreArchivo, ConvertFileToClass convertidor)
    {
        this._urlArchivo = directorio + "\\" + nombreArchivo;
        this._abierto = false;
        this._convertidor = convertidor;
    }
    
    private File AbrirArchivo() throws FileNotFoundException
    {
        File archivo = null;
        
        _abierto = true;
        archivo  = new File(this._urlArchivo);
        
        return archivo;
    }
    
    public String EscribirLinea(String linea)
    {
        String mensaje = null;
        File archivoDB = null;
        try
        {
            archivoDB = AbrirArchivo();
            FileWriter fEscribir=new FileWriter(archivoDB,true);
            fEscribir.write(linea);
            fEscribir.write("\n");
            fEscribir.close();
            _abierto = false;
        }
        catch(Exception ex)
        {
            mensaje = "Error al registrar archivo detalle: \n" + ex.getMessage();
        }
        
        return mensaje;
    }
    public String EscribirTodoArchivo(String[] lineas)
    {
        String mensaje = null;
        File archivoDB = null;
        try
        {
            archivoDB = AbrirArchivo();
            FileWriter fEscribir=new FileWriter(archivoDB,false);
            for(int i =0;i<lineas.length;i++)
            {
                fEscribir.write(lineas[i]);
                fEscribir.write("\n");
            }
            fEscribir.close();
            _abierto = false;
        }
        catch(Exception ex)
        {
            mensaje = "Error al registrar archivo detalle: \n" + ex.getMessage();
        }
        
        return mensaje;
    }
    
    public ArrayList GetListadoClases()
    {
        ArrayList lista = null;
        String linea = null;
        File archivoDB = null;
        try
        {
            archivoDB = AbrirArchivo();
            FileReader fLector=new FileReader(archivoDB);
            BufferedReader contenido=new BufferedReader(fLector);
            while((linea=contenido.readLine())!=null)
            {
                if(lista==null)
                    lista= this._convertidor.GetArrayListVacioInstancia();
                Object obj= this._convertidor.ConvertLineaToClase(linea);
                if(obj!=null)
                    lista.add(obj);
                else
                    break;
            }
        }
        catch(Exception ex)
        {
            lista = null;
        }
        return lista;
    }
    
}
