
package CompraVentaAuto.Herramienta;

import java.util.ArrayList;


public interface ConvertFileToClass {
    
    public abstract ArrayList GetArrayListVacioInstancia();
    public abstract Object ConvertLineaToClase(String lineaArchivo);
    public abstract ArrayList GetListArchivoDBClase();
    public abstract String Registrar();
    
}
